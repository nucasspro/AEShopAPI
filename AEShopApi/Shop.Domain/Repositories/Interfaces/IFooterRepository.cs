﻿using Shop.Domain.Entities;
using Shop.Domain.SeedWork;


namespace Shop.Domain.Repositories.Interfaces
{
    public interface IFooterRepository : IRepository<Footer>
    {
    }
}
