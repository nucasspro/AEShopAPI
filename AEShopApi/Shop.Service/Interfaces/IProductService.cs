﻿using Shop.Domain.Entities;
using Shop.Service.Interfaces;
using System.Collections.Generic;

namespace Shop.Service.Implements
{
    public interface IProductService : IService<Product>
    {
        
    }
}